#ifndef GRAFICA_FOURTH_H
#define GRAFICA_FOURTH_H
#include "fourth_lvl.h"
#include <QPainter>
#include <QGraphicsItem>
#include <QObject>
#include <QGraphicsPixmapItem>
#include <QGraphicsScene>


class GRAFICA_FOURTH:public QObject,
        public QGraphicsPixmapItem
{
Q_OBJECT

public:
    GRAFICA_FOURTH(QGraphicsItem* fourr = 0);
    void posicion(float v_lim);
    Fourth_lvl *getFour() const;
private:
    Fourth_lvl *four;
};

#endif // GRAFICA_FOURTH_H
