#include "grafica_pantalla_inicio.h"
#include "pantalla_inicio.h"




GRAFICA_PANTALLA_INICIO::GRAFICA_PANTALLA_INICIO(QGraphicsItem *pann):QGraphicsPixmapItem(pann)
{
    setPixmap(QPixmap(":/unnamed.png"));
    pan=new PANTALLA_INICIO;
}

PANTALLA_INICIO *GRAFICA_PANTALLA_INICIO::get_pan() const
{
    return pan;
}
