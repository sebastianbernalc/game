#-------------------------------------------------
#
# Project created by QtCreator 2019-04-14T19:39:23
#
#-------------------------------------------------

QT       += core gui serialport
QT       += multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Futbol
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        mainwindow.cpp \
    balon.cpp \
    grafica.cpp \
    fondo.cpp \
    graficafondo.cpp \
    pelota.cpp \
    graficapelota.cpp \
    porteria.cpp \
    graficaporteria.cpp \
    pantalla_inicial.cpp \
    pantalla_inicio.cpp \
    grafica_pantalla_inicio.cpp \
    registro.cpp \
    seleccion_de_juego.cpp \
    barrera.cpp \
    grafica_barrera.cpp \
    first_lvl.cpp \
    grafica_first_lvl.cpp \
    second_lvl.cpp \
    grafica_second_lvl.cpp \
    third_lvl.cpp \
    grafica_third.cpp \
    fourth_lvl.cpp \
    grafica_fourth.cpp \
    fondo_seleccion.cpp \
    grafica_fondo_seleccion.cpp

HEADERS += \
        mainwindow.h \
    balon.h \
    grafica.h \
    fondo.h \
    graficafondo.h \
    pelota.h \
    graficapelota.h \
    porteria.h \
    graficaporteria.h \
    pantalla_inicial.h \
    pantalla_inicio.h \
    grafica_pantalla_inicio.h \
    registro.h \
    seleccion_de_juego.h \
    barrera.h \
    grafica_barrera.h \
    first_lvl.h \
    grafica_first_lvl.h \
    second_lvl.h \
    grafica_second_lvl.h \
    third_lvl.h \
    grafica_third.h \
    fourth_lvl.h \
    grafica_fourth.h \
    fondo_seleccion.h \
    grafica_fondo_seleccion.h

FORMS += \
        mainwindow.ui \
    pantalla_inicial.ui \
    registro.ui \
    seleccion_de_juego.ui

RESOURCES += \
    objetos.qrc

DISTFILES += \
    Render_cristiano ronaldo real madrid 2013.png \
    New Project (2).png \
    New Project (2).png \
    New Project (3).png
