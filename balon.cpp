#include "balon.h"

BALON::BALON()
{

}

int BALON::getX() const
{
    return x;
}

int BALON::getY() const
{
    return y;
}

void BALON::setX(int value)
{
    x=value;
}

void BALON::setY(int value)
{
    y=value;
}

void BALON::setValues(int x1, int y1,int h1, int a1)
{
    x=x1;
    y=y1;
    h=h1;
    a=a1;
}

int BALON::getH() const
{
    return h;
}

int BALON::getA() const
{
    return a;
}

void BALON::setH(int value)
{
    h=value;
}

void BALON::setA(int value)
{
    a=value;
}
