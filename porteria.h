#ifndef PORTERIA_H
#define PORTERIA_H


class PORTERIA
{
public:
    PORTERIA();
    int getX() const;
    int getY() const;
    int getH() const;
    int getA() const;
    int getV()const;
    void setV(int value);
    void setX(int value);
    void setY(int value);
    void setH(int value);
    void setA(int value);
    void setValues(int x1, int y1, int h1, int a1);
    void movimiento();


private:
    float x=0, y=0, h=0, a=0, dt =1, v=2;
};

#endif // PORTERIA_H
