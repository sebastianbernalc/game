#include "graficapelota.h"



GRAFICAPELOTA::GRAFICAPELOTA(QGraphicsItem *pell):QGraphicsPixmapItem(pell)
{
    setPixmap(QPixmap(":/85276146-balón-de-fútbol-o-fútbol-icono-de-vector-plano-para-aplicaciones-de-deportes-y-sitios-web (2).png"));
    pel=new PELOTA;
}

void GRAFICAPELOTA::posicion(float v_lim)
{
    setPos(pel->getX(),(v_lim-pel->getY()-pel->getH()));
}

PELOTA *GRAFICAPELOTA::get_pel() const
{
    return pel;
}

void GRAFICAPELOTA::parabola()
{
    pel->setVelocidades();
    pel->parabola();
}

void GRAFICAPELOTA::rectilinio()
{
    pel->rectilinio();
}

void GRAFICAPELOTA::caida_libre()
{
    pel->caida_libre();
}

void GRAFICAPELOTA::circular()
{
    pel->circular();
}
