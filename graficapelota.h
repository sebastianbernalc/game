#ifndef GRAFICAPELOTA_H
#define GRAFICAPELOTA_H
#include <QPainter>
#include <QGraphicsItem>
#include <QObject>
#include <QGraphicsPixmapItem>
#include <QGraphicsScene>
#include  "pelota.h"


class GRAFICAPELOTA:public QObject,
        public QGraphicsPixmapItem
{
  Q_OBJECT
public:
    GRAFICAPELOTA(QGraphicsItem* pell = 0);
    void posicion(float v_lim);
    PELOTA *get_pel()const;
    void parabola();
    void rectilinio();
    void caida_libre();
    void circular();
private:
    PELOTA *pel;

};

#endif // GRAFICAPELOTA_H
