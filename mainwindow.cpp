#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "seleccion_de_juego.h"

#include<time.h>
#include<iostream>
using namespace std;
#include <stdlib.h>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    scene= new QGraphicsScene;
    scene->setSceneRect(0,0,h_limit,v_limit);     //asigna el rectangulo que encierra la scene, determinado por h_limit y v_limit
    ui->graphicsView->setScene(scene);
    controll=new QSerialPort; //Para saber que hace el arduino
    timer_control=new QTimer;




}
void MainWindow::keyPressEvent(QKeyEvent *event){       //Funciones de teclas
    sel=new Seleccion_de_juego;

    if(event->key()==Qt::Key_W && sel->get_Selec()==false){  //Si el evento o tecla es w
        if(porte->getPor()->getY()!=640-porte->getPor()->getH()){  //condicion para que al presionar la tecla haya movimiento

        porte->getPor()->setY(porte->getPor()->getY()+5);
        porte->posicion(v_limit);
        }
    };
    if(event->key()==Qt::Key_S && sel->get_Selec()==false){ //si la tecla es s
        if(porte->getPor()->getY()!=0){porte->getPor()->setY(porte->getPor()->getY()-5);
        porte->posicion(v_limit);}
    };

}

MainWindow::~MainWindow()
{
    delete ui;
}



void MainWindow::on_pushButton_2_clicked()
{
    pelota->get_pel()->setV((ui->lcdNumber->intValue()));  //Le da el valor a la velocidad por medio del dial

    pelota->get_pel()->setAN(ui->lcdNumber_2->intValue());  //Angulo por medio de dial2
    pelota->get_pel()->setVelocidades();    //Actualiza las velocidades o las inicia
    pelota->get_pel()->parabola();
    med=new QMediaPlayer;       //Para dar sonido al juego
    med->setMedia(QUrl::fromLocalFile("/Users/MSI-PC/Desktop/Game/Futbol/y2mate.com - ahi_esta_gool_mariano_closs_-M-xbwWeA5k-[AudioTrimmer.com]-[AudioTrimmer.com].mp3"));
    med->setVolume(80);
    med->play();
    mov->start(20);

}

void MainWindow::fun_mov2()
{
    if(porte->getPor()->getY()>=0){     //todas estas condiciones son para el movimiento de la barrera y la porteria para que en cada rango puedan moverse
        porte->movimiento();            //Todo es para el nivel 2
        porte->posicion(v_limit);}
    if(porte->getPor()->getY()>441){
        porte->getPor()->setV(-porte->getPor()->getV());
        porte->movimiento();
        porte->posicion(v_limit);
    }
    if(porte->getPor()->getY()<0){
        porte->getPor()->setV(-porte->getPor()->getV());
        porte->movimiento();
        porte->posicion(v_limit);
    }

    if(barrera->getBar()->getX()>=500){
        barrera->movimiento();
        barrera->posicion(v_limit);
    }
    if(barrera->getBar()->getX()>1100){
        barrera->getBar()->setV(-barrera->getBar()->getV());
        barrera->movimiento();
        barrera->posicion(v_limit);
    }
    if(barrera->getBar()->getX()<500){

            barrera->getBar()->setV(-barrera->getBar()->getV());
            barrera->movimiento();
            barrera->posicion(v_limit);

    }



}

void MainWindow::fun_tercerlvl()
{
    if(porte->getPor()->getY()>=0){         //Movimiento de obstaculos aleatorios en un determinado rango para el lvl 3
        porte->movimiento();
        porte->posicion(v_limit);}
    if(porte->getPor()->getY()>441){
        porte->getPor()->setV(-porte->getPor()->getV());
        porte->movimiento();
        porte->posicion(v_limit);
    }
    if(porte->getPor()->getY()<0){
        porte->getPor()->setV(-porte->getPor()->getV());
        porte->movimiento();
        porte->posicion(v_limit);
    }

    if(barrera->getBar()->getX()>=500){
        barrera->movimiento();
        barrera->posicion(v_limit);
    }
    if(barrera->getBar()->getX()>1100){
        barrera->getBar()->setV(-barrera->getBar()->getV());
        barrera->movimiento();
        barrera->posicion(v_limit);
    }
    if(barrera->getBar()->getX()<500){

            barrera->getBar()->setV(-barrera->getBar()->getV());
            barrera->movimiento();
            barrera->posicion(v_limit);

    }
// SOLO PARA 3ER LVL
    if(pel1->get_pel()->getY()<=100){
        pel1->rectilinio();
        pel1->posicion(v_limit);
    }

    if(pel1->get_pel()->getY()<-530){
        pel1->get_pel()->setVEL(-pel1->get_pel()->getVEL());
        pel1->rectilinio();
        pel1->posicion(v_limit);
    }

    if(pel1->get_pel()->getY()>100){
        pel1->get_pel()->setVEL(-pel1->get_pel()->getVEL());
        pel1->rectilinio();
        pel1->posicion(v_limit);
    }
    if(pel2->get_pel()->getY()<=100){pel2->caida_libre();
    pel2->posicion(v_limit);}

    if(pel2->get_pel()->getY()<-530){
        pel2->get_pel()->setVEL(-pel2->get_pel()->getVEL());
        pel2->caida_libre();
        pel2->posicion(v_limit);
    }
    if(pel2->get_pel()->getY()>100){
        pel2->get_pel()->setVEL(-pel2->get_pel()->getVEL());
        pel2->caida_libre();
        pel2->posicion(v_limit);
    }

    if(pel3->get_pel()->getY()<=100){
        pel3->get_pel()->setVEL(pel3->get_pel()->getVEL()+2);
        pel3->caida_libre();
        pel3->posicion(v_limit);}

    if(pel3->get_pel()->getY()<-530){
        pel3->get_pel()->setVEL(-pel3->get_pel()->getVEL());
        pel3->caida_libre();
        pel3->posicion(v_limit);
    }
    if(pel3->get_pel()->getY()>100){
        pel3->get_pel()->setVEL(-pel3->get_pel()->getVEL());
        pel3->caida_libre();
        pel3->posicion(v_limit);
    }
}

void MainWindow::fun_tercer_coll()
{
    if(pelota->get_pel()->getY()<530 && pelota->get_pel()->getX()<1320&&pelota->get_pel()->getY()>-98){

                pelota->get_pel()->parabola();
                pelota->posicion(v_limit);
                                                            //Determinacion de colisiones con los demas objetos para que reboten y que pasa si el balon colisiona con los obstaculos
            }

            else{
            pelota->get_pel()->setvy(-pelota->get_pel()->getvy());
            pelota->get_pel()->parabola();
            pelota->posicion(v_limit);
                 }
            if(pelota->get_pel()->getX()>1310){
                tiros=tiros+1;
                tiros_total++;
                pelota->get_pel()->setValues(80,-35,110,110);
                pelota->posicion(v_limit);
                mov->stop();
                if(tiros==3){  //Aqui nos cuenta cuantos tiros puede hacer por cada nivel en este caso 3
                    lvl++;
                    fun_Nivel(lvl);
                    tiros=0;
                }
            }
            if(pelota->collidesWithItem(porte)){
            puntaje+=10;
            on_progressBar_valueChanged(puntaje);
            cout<<puntaje<<endl;
            med=new QMediaPlayer; //Cada vez que haga gol sonara un efecto
            med->setMedia(QUrl::fromLocalFile("/Users/MSI-PC/Desktop/Game/Futbol/y2mate.com - ahi_esta_gool_mariano_closs_-M-xbwWeA5k-[AudioTrimmer.com] (1).mp3"));
            med->setVolume(80);
            med->play();
            mov->start(20);
            }

            if(pelota->collidesWithItem(barrera)){
                tiros++;
                tiros_total++;
                cout<<"tiros"<<endl;
                pelota->get_pel()->setValues(80,-35,110,110);
                pelota->posicion(v_limit);
                mov->stop();
                if(tiros==3){
                    lvl++;
                    fun_Nivel(lvl);
                    tiros=0;
                }
            }
//SOLO TERCER LVL
            if(pelota->collidesWithItem(pel1)){
                tiros=tiros+1;
                tiros_total++;
                pelota->get_pel()->setValues(80,-35,110,110);
                pelota->posicion(v_limit);
                mov->stop();
                if(tiros==3){
                    lvl++;
                    fun_Nivel(lvl);
                    tiros=0;
                };
            }
            if(pelota->collidesWithItem(pel2)){
                tiros=tiros+1;
                tiros_total++;
                pelota->get_pel()->setValues(80,-35,110,110);
                pelota->posicion(v_limit);
                mov->stop();
                if(tiros==3){
                    lvl++;
                    fun_Nivel(lvl);
                    tiros=0;
                }
            }

            if(pelota->collidesWithItem(pel3)){
                tiros=tiros+1;
                tiros_total++;
                pelota->get_pel()->setValues(80,-35,110,110);
                pelota->posicion(v_limit);
                mov->stop();
                if(tiros==3){
                    lvl++;
                    fun_Nivel(lvl);
                    tiros=0;
                }
            }

}

void MainWindow::fun_cuartolvl()
{
    if(porte->getPor()->getY()>=0){ //movimiento de obstaculos con condiciones para rebote entre otras
        porte->movimiento();
        porte->posicion(v_limit);}
    if(porte->getPor()->getY()>441){
        porte->getPor()->setV(-porte->getPor()->getV());
        porte->movimiento();
        porte->posicion(v_limit);
    }
    if(porte->getPor()->getY()<0){
        porte->getPor()->setV(-porte->getPor()->getV());
        porte->movimiento();
        porte->posicion(v_limit);
    }

    if(barrera->getBar()->getX()>=500){
        barrera->movimiento();
        barrera->posicion(v_limit);
    }
    if(barrera->getBar()->getX()>1100){
        barrera->getBar()->setV(-barrera->getBar()->getV());
        barrera->movimiento();
        barrera->posicion(v_limit);
    }
    if(barrera->getBar()->getX()<500){

            barrera->getBar()->setV(-barrera->getBar()->getV());
            barrera->movimiento();
            barrera->posicion(v_limit);

    }

    pel4->circular();
    pel4->posicion(v_limit);

    pel5->get_pel()->setcons(1);
    pel5->get_pel()->setVEL(35);
    pel5->circular();
    pel5->posicion(v_limit);

    pel6->get_pel()->setcons(2);
    pel6->get_pel()->setVEL(40);
    pel6->circular();
    pel6->posicion(v_limit);

    pel7->get_pel()->setcons(2);
    pel7->get_pel()->setVEL(40);
    pel7->circular();
    pel7->posicion(v_limit);

}

void MainWindow::fun_cuarto_coll()
{
       if(pelota->get_pel()->getY()<530 && pelota->get_pel()->getX()<1320&&pelota->get_pel()->getY()>-98){

                pelota->get_pel()->parabola();
                pelota->posicion(v_limit);

            }

            else{
            pelota->get_pel()->setvy(-pelota->get_pel()->getvy());
            pelota->get_pel()->parabola();
            pelota->posicion(v_limit);
                 }
            if(pelota->get_pel()->getX()>1310){
                tiros=tiros+1;
                tiros_total++;
                pelota->get_pel()->setValues(80,-35,110,110);
                pelota->posicion(v_limit);
                mov->stop();
                if(tiros==3){


                    //PASAR DE PANTALLA
                }
            }
            if(pelota->collidesWithItem(porte)){
            puntaje+=10;
            on_progressBar_valueChanged(puntaje);
            cout<<puntaje<<endl;
            med=new QMediaPlayer;
            med->setMedia(QUrl::fromLocalFile("/Users/MSI-PC/Desktop/Game/Futbol/y2mate.com - ahi_esta_gool_mariano_closs_-M-xbwWeA5k-[AudioTrimmer.com] (1).mp3"));
            med->setVolume(80);
            med->play();
            mov->start(20);
            }

            if(pelota->collidesWithItem(barrera)){

                tiros=tiros+1;
                tiros_total++;
                pelota->get_pel()->setValues(80,-35,110,110);
                pelota->posicion(v_limit);
                mov->stop();
                if(tiros>=3){
                    scene->removeItem(balon);
                    scene->removeItem(pelota);
                    scene->removeItem(pel7);
                    scene->removeItem(pel6);
                    scene->removeItem(pel5);
                    scene->removeItem(pel4);
                    scene->removeItem(porte);
                    scene->removeItem(barrera);

                }
            }

            if(pelota->collidesWithItem(pel4)){
                tiros=tiros+1;
                tiros_total++;
                pelota->get_pel()->setValues(80,-35,110,110);
                pelota->posicion(v_limit);
                mov->stop();
                if(tiros>=3){

                    scene->removeItem(balon);
                    scene->removeItem(pelota);
                    scene->removeItem(pel7);
                    scene->removeItem(pel6);
                    scene->removeItem(pel5);
                    scene->removeItem(pel4);
                    scene->removeItem(porte);
                    scene->removeItem(barrera);

                }
            }
            if(pelota->collidesWithItem(pel5)){
                tiros=tiros+1;
                tiros_total++;
                pelota->get_pel()->setValues(80,-35,110,110);
                pelota->posicion(v_limit);
                mov->stop();
                if(tiros>=3){

                    scene->removeItem(balon);
                    scene->removeItem(pelota);
                    scene->removeItem(pel7);
                    scene->removeItem(pel6);
                    scene->removeItem(pel5);
                    scene->removeItem(pel4);
                    scene->removeItem(porte);
                    scene->removeItem(barrera);

                }
            }
            if(pelota->collidesWithItem(pel6)){
                tiros=tiros+1;
                tiros_total++;
                pelota->get_pel()->setValues(80,-35,110,110);
                pelota->posicion(v_limit);
                mov->stop();
                if(tiros>=3){

                    scene->removeItem(balon);
                    scene->removeItem(pelota);
                    scene->removeItem(pel7);
                    scene->removeItem(pel6);
                    scene->removeItem(pel5);
                    scene->removeItem(pel4);
                    scene->removeItem(porte);
                    scene->removeItem(barrera);

                }
            }
            if(pelota->collidesWithItem(pel7)){
                tiros=tiros+1;
                tiros_total++;
                pelota->get_pel()->setValues(80,-35,110,110);
                pelota->posicion(v_limit);
                mov->stop();
                if(tiros>=3){
                    scene->removeItem(balon);
                    scene->removeItem(pelota);
                    scene->removeItem(pel7);
                    scene->removeItem(pel6);
                    scene->removeItem(pel5);
                    scene->removeItem(pel4);
                    scene->removeItem(porte);
                    scene->removeItem(barrera);


                }
            }

}

void MainWindow::fun_cuartolvl_Muilti()
{

    pel4->circular();       //GRAFICA LOS OBSTACULOS DEL CUARTO NIVEL SIN NINGUNA CONDICION DE REBOTE PORQUE ES MOVIMIENTO CIRCULAR UNIFORME
    pel4->posicion(v_limit);

    pel5->get_pel()->setcons(1);
    pel5->get_pel()->setVEL(35);
    pel5->circular();
    pel5->posicion(v_limit);

    pel6->get_pel()->setcons(2);
    pel6->get_pel()->setVEL(40);
    pel6->circular();
    pel6->posicion(v_limit);

    pel7->get_pel()->setcons(2);
    pel7->get_pel()->setVEL(40);
    pel7->circular();
    pel7->posicion(v_limit);

}

void MainWindow::fun_tercerlvl_Multi()
{
    if(pel1->get_pel()->getY()<=100){       //CONDICIONES DE MOVIMIENTO PARA OBSTACULOS Y OBJETOS DEL TERCER LVL CON REBOTE PARA LO QUE LO NECESITEN
        pel1->rectilinio();
        pel1->posicion(v_limit);
    }

    if(pel1->get_pel()->getY()<-530){
        pel1->get_pel()->setVEL(-pel1->get_pel()->getVEL());
        pel1->rectilinio();
        pel1->posicion(v_limit);
    }

    if(pel1->get_pel()->getY()>100){
        pel1->get_pel()->setVEL(-pel1->get_pel()->getVEL());
        pel1->rectilinio();
        pel1->posicion(v_limit);
    }
    if(pel2->get_pel()->getY()<=100){pel2->caida_libre();
    pel2->posicion(v_limit);}

    if(pel2->get_pel()->getY()<-530){
        pel2->get_pel()->setVEL(-pel2->get_pel()->getVEL());
        pel2->caida_libre();
        pel2->posicion(v_limit);
    }
    if(pel2->get_pel()->getY()>100){
        pel2->get_pel()->setVEL(-pel2->get_pel()->getVEL());
        pel2->caida_libre();
        pel2->posicion(v_limit);
    }

    if(pel3->get_pel()->getY()<=100){
        pel3->get_pel()->setVEL(pel3->get_pel()->getVEL()+2);
        pel3->caida_libre();
        pel3->posicion(v_limit);}

    if(pel3->get_pel()->getY()<-530){
        pel3->get_pel()->setVEL(-pel3->get_pel()->getVEL());
        pel3->caida_libre();
        pel3->posicion(v_limit);
    }
    if(pel3->get_pel()->getY()>100){
        pel3->get_pel()->setVEL(-pel3->get_pel()->getVEL());
        pel3->caida_libre();
        pel3->posicion(v_limit);
    }
}

void MainWindow::fun_mov2_Multi()
{
    if(barrera->getBar()->getX()>=500){  //AQUI SE HACE QUE LA BARRERA SE MUEVA EN UN RANGO PRE DETERMINADO PARA QUE HAYA MAS DIFICULTAD
        barrera->movimiento();
        barrera->posicion(v_limit);
    }
    if(barrera->getBar()->getX()>1100){
        barrera->getBar()->setV(-barrera->getBar()->getV());
        barrera->movimiento();
        barrera->posicion(v_limit);
    }
    if(barrera->getBar()->getX()<500){

            barrera->getBar()->setV(-barrera->getBar()->getV());
            barrera->movimiento();
            barrera->posicion(v_limit);

    }
}

void MainWindow::fun_mov_Multi()
{


   if(pelota->get_pel()->getY()<530 && pelota->get_pel()->getX()<1320&&pelota->get_pel()->getY()>-98){

                pelota->get_pel()->parabola();
                pelota->posicion(v_limit);                      //SON LAS MISMAS FUNCIONES PARA UN SOLO JUGADOR SOLO QUE LA BARRERA SERA ESTATICA Y NADA MAS SERA CONTROLADA POR EL JUGADOR

            }

            else{
            pelota->get_pel()->setvy(-pelota->get_pel()->getvy());
            pelota->get_pel()->parabola();
            pelota->posicion(v_limit);
                 }
  if(c==0){puntaje=0;}
   if(pelota->collidesWithItem(porte)){

   puntaje=puntaje+10;
   on_progressBar_valueChanged(puntaje);
   cout<<puntaje<<endl;
   med=new QMediaPlayer;
   med->setMedia(QUrl::fromLocalFile("/Users/MSI-PC/Desktop/Game/Futbol/y2mate.com - ahi_esta_gool_mariano_closs_-M-xbwWeA5k-[AudioTrimmer.com] (1).mp3"));
   med->setVolume(80);
   med->play();
   mov->start(20);
   }

   if(pelota->get_pel()->getX()>=1310){

                tiros=tiros+1;
                cout<<"tiros "<<tiros<<endl;
                pelota->get_pel()->setValues(80,-35,110,110);
                pelota->posicion(v_limit);
                mov->stop();
                if(tiros==3){
                    lvl++;
                    fun_Nivel_Multijugador(lvl);
                    tiros=0;
                }

            }

            if(pelota->collidesWithItem(barrera)){
                tiros=tiros+1;

                pelota->get_pel()->setValues(80,-35,110,110);
                pelota->posicion(v_limit);
                mov->stop();
                if(tiros==3){
                    lvl++;
                    fun_Nivel_Multijugador(lvl);
                    tiros=0;
                }
            }
c++;
}

void MainWindow::fun_tercer_coll_Multi()
{
    if(pelota->get_pel()->getY()<530 && pelota->get_pel()->getX()<1320&&pelota->get_pel()->getY()>-98){

                pelota->get_pel()->parabola();
                pelota->posicion(v_limit);

            }               //MISMA FUNCION SOLO QUE PARA MULTIJUGADOR CON PORTERIA ESTATICA MANIPULADA POR EL SEGUNDO JUGADOR

            else{
            pelota->get_pel()->setvy(-pelota->get_pel()->getvy());
            pelota->get_pel()->parabola();
            pelota->posicion(v_limit);
                 }
            if(pelota->get_pel()->getX()>1310){
                tiros=tiros+1;
                tiros_total++;
                pelota->get_pel()->setValues(80,-35,110,110);
                pelota->posicion(v_limit);
                mov->stop();
                if(tiros==3){
                    lvl++;
                    fun_Nivel_Multijugador(lvl);
                    tiros=0;
                }
            }
            if(pelota->collidesWithItem(porte)){
            puntaje=puntaje+10;
            on_progressBar_valueChanged(puntaje);
            cout<<puntaje<<endl;
            med=new QMediaPlayer;
            med->setMedia(QUrl::fromLocalFile("/Users/MSI-PC/Desktop/Game/Futbol/y2mate.com - ahi_esta_gool_mariano_closs_-M-xbwWeA5k-[AudioTrimmer.com] (1).mp3"));
            med->setVolume(80);
            med->play();
            mov->start(20);
            }

            if(pelota->collidesWithItem(barrera)){
                tiros=tiros+1;
                tiros_total++;
                pelota->get_pel()->setValues(80,-35,110,110);
                pelota->posicion(v_limit);
                mov->stop();
                if(tiros==3){
                    lvl++;
                    fun_Nivel_Multijugador(lvl);
                    tiros=0;
                }
            }
//SOLO TERCER LVL
            if(pelota->collidesWithItem(pel1)){
                tiros=tiros+1;
                tiros_total++;
                pelota->get_pel()->setValues(80,-35,110,110);
                pelota->posicion(v_limit);
                mov->stop();
                if(tiros==3){
                    lvl++;
                    fun_Nivel_Multijugador(lvl);
                    tiros=0;
                };
            }
            if(pelota->collidesWithItem(pel2)){
                tiros=tiros+1;
                tiros_total++;
                pelota->get_pel()->setValues(80,-35,110,110);
                pelota->posicion(v_limit);
                mov->stop();
                if(tiros==3){
                    lvl++;
                    fun_Nivel_Multijugador(lvl);
                    tiros=0;
                }
            }

            if(pelota->collidesWithItem(pel3)){
                tiros=tiros+1;
                tiros_total++;
                pelota->get_pel()->setValues(80,-35,110,110);
                pelota->posicion(v_limit);
                mov->stop();
                if(tiros==3){
                    lvl++;
                    fun_Nivel_Multijugador(lvl);
                    tiros=0;
                }
            }
}

void MainWindow::conectar_arduino()
{
    controll->setPortName("COM5"); //INICIA EL ARDUINO POR EL PUERTO
    //timer_control->start(100);
    if(controll->open(QIODevice::ReadWrite)){
        //Ahora el puerto seria está abierto
        if(!controll->setBaudRate(QSerialPort::Baud9600)) //Configurar la tasa de baudios
            qDebug()<<controll->errorString();

        if(!controll->setDataBits(QSerialPort::Data8))
            qDebug()<<controll->errorString();

        if(!controll->setParity(QSerialPort::NoParity))
            qDebug()<<controll->errorString();

        if(!controll->setStopBits(QSerialPort::OneStop))
            qDebug()<<controll->errorString();

        if(!controll->setFlowControl(QSerialPort::NoFlowControl))
            qDebug()<<controll->errorString();
            timer_control->start(1);
            qDebug()<<"Serial connect "<<endl;
    }
    else{
        qDebug()<<"Serial COM3 not opened. Error: "<<controll->errorString();
    }
}

void MainWindow::control()
{
    char data;
    int l = 0;
    bool flag=true;
       if(controll->waitForReadyRead(1500)){

            //Data was returned
            l = controll->read(&data,1);
            switch (data) {
            case 'A':
                  ui->dial->setValue(ui->dial->value()+1); //AL VALOR INICIAL DEL DIAL LE SUMA 1 CADA VEZ QUE SE PRESIONE EL BOTON


                break;
            case 'B':
                 ui->dial->setValue(ui->dial->value()-1); //LE RESTA 1
                break;
            case 'C':
                  ui->dial_2->setValue(ui->dial_2->value()+1); //SUMA 1 AL DIAL2
                break;
            case 'D':
                  ui->dial_2->setValue(ui->dial_2->value()-1); //LE RESTA 1
                break;
            case 'K':
                ui->pushButton_2->click(); //PATEA AL PRESIONAR EL BOTON
            case 'N':
                break;
            default:
                break;
            }

            qDebug()<<"Response: "<<data;
            flag=false;

        }else{
            //No data
           porte->posicion(v_limit);
           qDebug()<<"Time out";
      }
    //}
    //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    //control->close();



}

void MainWindow::fun_cuarto_coll_Multi()
{

}

void MainWindow::fun_Nivel(int value)
{
    med=new QMediaPlayer;  //MUSICA PARA EL PRIMER LVL
    med->setMedia(QUrl::fromLocalFile("/Users/MSI-PC/Desktop/Game/Futbol/y2mate.com - ambiente_partido_de_futbol_efectos_de_sonido_clTnEY3nKvc.mp3"));
    med->setVolume(80);
    med->play();

    if(value==1){

     fondo=new GRAFICAFONDO;  //FONDO DEL JUEGO
     fondo->get_fon()->setValues(100,100);
     scene->addItem(fondo);
     first=new GRAFICA_FIRST_LVL; //IMAGEN DE FIRST LEVEL
     first->getFir()->setValues(500,300,300,100);
     first->posicion(v_limit);
     scene->addItem(first);




     balon = new GRAFICA;    //CR7 JUGADOR
     balon->get_bal()->setValues(-60,0,200,80);
     balon->posicion(v_limit);
     scene->addItem(balon);
     porte=new GRAFICAPORTERIA; //PORTERIA
     porte->getPor()->setValues(1310,(400),200,30);
     porte->posicion(v_limit);
     scene->addItem(porte);
     pelota=new GRAFICAPELOTA; //BALON
     pelota->get_pel()->setValues(80,-35,110,110);
     pelota->posicion(v_limit);
     scene->addItem(pelota);
     barrera=new GRAFICA_BARRERA; //BARRERA
     barrera->getBar()->setValues(500,50,128,30);
     barrera->posicion(v_limit);
     scene->addItem(barrera);

     mov=new QTimer();
     connect(mov,SIGNAL(timeout()),this,SLOT(fun_mov()));
     controll=new QSerialPort;
     timer_control=new QTimer;
     conectar_arduino();
     connect(timer_control,SIGNAL(timeout()),this,SLOT(control()));





    }

    tiros=0;
     //SEGUNDO NIVEL

    if(value==2){
     fondo=new GRAFICAFONDO;
     fondo->get_fon()->setValues(100,100);
     scene->addItem(fondo);
     second=new GRAFICA_SECOND_LVL;
     second->getSec()->setValues(500,300,300,100);
     second->posicion(v_limit);
     scene->addItem(second);



     balon = new GRAFICA;
     balon->get_bal()->setValues(-60,0,200,80);
     balon->posicion(v_limit);
     scene->addItem(balon);
     porte=new GRAFICAPORTERIA;
     porte->getPor()->setValues(1310,0,200,30);
     porte->posicion(v_limit);
     scene->addItem(porte);
     pelota=new GRAFICAPELOTA;
     pelota->get_pel()->setValues(80,-35,110,110);
     pelota->posicion(v_limit);
     scene->addItem(pelota);
     barrera=new GRAFICA_BARRERA;
     barrera->getBar()->setValues(500,50,128,30);
     barrera->posicion(v_limit);
     scene->addItem(barrera);
     mov2=new QTimer();
     mov2->start(20);
     connect(mov2,SIGNAL(timeout()),this,SLOT(fun_mov2()));

     mov=new QTimer();
     connect(mov,SIGNAL(timeout()),this,SLOT(fun_mov()));}

     //TERCER NIVEL

    if(value==3){

         fondo=new GRAFICAFONDO;
         fondo->get_fon()->setValues(100,100);
         scene->addItem(fondo);
         pel1= new GRAFICAPELOTA;
         pel1->get_pel()->setValues(0,100,541,110);
         pel1->posicion(v_limit);
         scene->addItem(pel1);
         pel2= new GRAFICAPELOTA;
         pel2->get_pel()->setValues(1200,100,541,110);
         pel2->posicion(v_limit);
         scene->addItem(pel2);
         pel3= new GRAFICAPELOTA;
         pel3->get_pel()->setValues(800,100,541,110);
         pel3->posicion(v_limit);
         scene->addItem(pel3);
         third=new GRAFICA_THIRD;
         third->get_thir()->setValues(500,300,300,100);
         third->posicion(v_limit);
         scene->addItem(third);

         balon = new GRAFICA;
         balon->get_bal()->setValues(-60,0,200,80);
         balon->posicion(v_limit);
         scene->addItem(balon);
         porte=new GRAFICAPORTERIA;
         porte->getPor()->setValues(1310,0,200,30);
         porte->posicion(v_limit);
         scene->addItem(porte);
         pelota=new GRAFICAPELOTA;
         pelota->get_pel()->setValues(80,-35,110,110);
         pelota->posicion(v_limit);
         scene->addItem(pelota);
         barrera=new GRAFICA_BARRERA;
         barrera->getBar()->setValues(500,50,128,30);
         barrera->posicion(v_limit);
         scene->addItem(barrera);
         mov2=new QTimer();
         mov2->start(20);
         connect(mov2,SIGNAL(timeout()),this,SLOT(fun_tercerlvl()));

         mov=new QTimer();
         connect(mov,SIGNAL(timeout()),this,SLOT(fun_tercer_coll())); }

     //CUARTO LVL
        if(value==4){

            fondo=new GRAFICAFONDO;
             fondo->get_fon()->setValues(100,100);
             scene->addItem(fondo);

             pel4= new GRAFICAPELOTA;
             pel4->get_pel()->setValues(800,-98,541,110);
             pel4->posicion(v_limit);
             scene->addItem(pel4);

             pel5= new GRAFICAPELOTA;
             pel5->get_pel()->setValues(600,-400,541,110);
             pel5->posicion(v_limit);
             scene->addItem(pel5);
             pel6= new GRAFICAPELOTA;
             pel6->get_pel()->setValues(750,-430,541,110);
             pel6->posicion(v_limit);
             scene->addItem(pel6);
             pel7= new GRAFICAPELOTA;
             pel7->get_pel()->setValues(1000,-330,541,110);
             pel7->posicion(v_limit);
             scene->addItem(pel7);
             fourth=new GRAFICA_FOURTH;
             fourth->getFour()->setValues(500,300,300,100);
             fourth->posicion(v_limit);
             scene->addItem(fourth);

             balon = new GRAFICA;
             balon->get_bal()->setValues(-60,0,200,80);
             balon->posicion(v_limit);
             scene->addItem(balon);
             porte=new GRAFICAPORTERIA;
             porte->getPor()->setValues(1310,0,200,30);
             porte->posicion(v_limit);
             scene->addItem(porte);
             pelota=new GRAFICAPELOTA;
             pelota->get_pel()->setValues(80,-35,110,110);
             pelota->posicion(v_limit);
             scene->addItem(pelota);
             barrera=new GRAFICA_BARRERA;
             barrera->getBar()->setValues(500,50,128,30);
             barrera->posicion(v_limit);
             scene->addItem(barrera);
             mov2=new QTimer();
             mov2->start(20);
             connect(mov2,SIGNAL(timeout()),this,SLOT(fun_cuartolvl()));

             mov=new QTimer();
             connect(mov,SIGNAL(timeout()),this,SLOT(fun_cuarto_coll()));

 }

}

void MainWindow::fun_Nivel_Multijugador(int value)
{
    if(value==1){   //SELECCION DE NIVELES EN MODO MULTIJUGADOR, AGREGA CADA UNO DE LOS OBJETOS Y CONECTA SUS FUNCIONES DE MOVIMIENTO

     fondo=new GRAFICAFONDO;
     fondo->get_fon()->setValues(100,100);
     scene->addItem(fondo);


     srand(time(NULL));

     barrera_Aleatoria=200+rand()%(641);
     balon = new GRAFICA;
     balon->get_bal()->setValues(-60,0,200,80);
     balon->posicion(v_limit);
     scene->addItem(balon);
     porte=new GRAFICAPORTERIA;
     porte->getPor()->setValues(1310,(barrera_Aleatoria-200),200,30);
     porte->posicion(v_limit);
     scene->addItem(porte);
     pelota=new GRAFICAPELOTA;
     pelota->get_pel()->setValues(80,-35,110,110);
     pelota->posicion(v_limit);
     scene->addItem(pelota);
     barrera=new GRAFICA_BARRERA;
     barrera->getBar()->setValues(500,50,128,30);
     barrera->posicion(v_limit);
     scene->addItem(barrera);
     if(puntaje=!0){puntaje=0;}
     mov=new QTimer();
     connect(mov,SIGNAL(timeout()),this,SLOT(fun_mov_Multi()));


    }

    tiros=0;
     //SEGUNDO NIVEL

    if(value==2){
     fondo=new GRAFICAFONDO;
     fondo->get_fon()->setValues(100,100);
     scene->addItem(fondo);





     balon = new GRAFICA;
     balon->get_bal()->setValues(-60,0,200,80);
     balon->posicion(v_limit);
     scene->addItem(balon);
     porte=new GRAFICAPORTERIA;
     porte->getPor()->setValues(1310,0,200,30);
     porte->posicion(v_limit);
     scene->addItem(porte);
     pelota=new GRAFICAPELOTA;
     pelota->get_pel()->setValues(80,-35,110,110);
     pelota->posicion(v_limit);
     scene->addItem(pelota);
     barrera=new GRAFICA_BARRERA;
     barrera->getBar()->setValues(500,50,128,30);
     barrera->posicion(v_limit);
     scene->addItem(barrera);
     mov2=new QTimer();
     mov2->start(20);
     connect(mov2,SIGNAL(timeout()),this,SLOT(fun_mov2_Multi()));

     mov=new QTimer();
     connect(mov,SIGNAL(timeout()),this,SLOT(fun_mov_Multi()));}

     //TERCER NIVEL

    if(value==3){

         fondo=new GRAFICAFONDO;
         fondo->get_fon()->setValues(100,100);
         scene->addItem(fondo);
         pel1= new GRAFICAPELOTA;
         pel1->get_pel()->setValues(0,100,541,110);
         pel1->posicion(v_limit);
         scene->addItem(pel1);
         pel2= new GRAFICAPELOTA;
         pel2->get_pel()->setValues(1200,100,541,110);
         pel2->posicion(v_limit);
         scene->addItem(pel2);
         pel3= new GRAFICAPELOTA;
         pel3->get_pel()->setValues(800,100,541,110);
         pel3->posicion(v_limit);
         scene->addItem(pel3);

         balon = new GRAFICA;
         balon->get_bal()->setValues(-60,0,200,80);
         balon->posicion(v_limit);
         scene->addItem(balon);
         porte=new GRAFICAPORTERIA;
         porte->getPor()->setValues(1310,0,200,30);
         porte->posicion(v_limit);
         scene->addItem(porte);
         pelota=new GRAFICAPELOTA;
         pelota->get_pel()->setValues(80,-35,110,110);
         pelota->posicion(v_limit);
         scene->addItem(pelota);
         barrera=new GRAFICA_BARRERA;
         barrera->getBar()->setValues(500,50,128,30);
         barrera->posicion(v_limit);
         scene->addItem(barrera);
         mov2=new QTimer();
         mov2->start(20);
         connect(mov2,SIGNAL(timeout()),this,SLOT(fun_tercerlvl_Multi()));

         mov=new QTimer();
         connect(mov,SIGNAL(timeout()),this,SLOT(fun_tercer_coll_Multi())); }

     //CUARTO LVL
        if(value==4){
             fondo=new GRAFICAFONDO;
             fondo->get_fon()->setValues(100,100);
             scene->addItem(fondo);

             pel4= new GRAFICAPELOTA;
             pel4->get_pel()->setValues(800,-98,541,110);
             pel4->posicion(v_limit);
             scene->addItem(pel4);

             pel5= new GRAFICAPELOTA;
             pel5->get_pel()->setValues(600,-400,541,110);
             pel5->posicion(v_limit);
             scene->addItem(pel5);
             pel6= new GRAFICAPELOTA;
             pel6->get_pel()->setValues(750,-430,541,110);
             pel6->posicion(v_limit);
             scene->addItem(pel6);
             pel7= new GRAFICAPELOTA;
             pel7->get_pel()->setValues(1000,-330,541,110);
             pel7->posicion(v_limit);
             scene->addItem(pel7);

             balon = new GRAFICA;
             balon->get_bal()->setValues(-60,0,200,80);
             balon->posicion(v_limit);
             scene->addItem(balon);
             porte=new GRAFICAPORTERIA;
             porte->getPor()->setValues(1310,0,200,30);
             porte->posicion(v_limit);
             scene->addItem(porte);
             pelota=new GRAFICAPELOTA;
             pelota->get_pel()->setValues(80,-35,110,110);
             pelota->posicion(v_limit);
             scene->addItem(pelota);
             barrera=new GRAFICA_BARRERA;
             barrera->getBar()->setValues(500,50,128,30);
             barrera->posicion(v_limit);
             scene->addItem(barrera);
             mov2=new QTimer();
             mov2->start(20);
             connect(mov2,SIGNAL(timeout()),this,SLOT(fun_cuartolvl_Muilti()));

             mov=new QTimer();
             connect(mov,SIGNAL(timeout()),this,SLOT(fun_cuarto_coll()));
}
}


void MainWindow::Pasar_Lvl(int value)
{
    lvl=value;  //FUNCION PARA PASAR VARIABLE DE UNA CLASE A OTRA
}

void MainWindow::tener_datos_finales(string usuario_cargado)
{
    usuario_cargado_final=usuario_cargado;
}




void MainWindow::on_pushButton_clicked() //boton para volver a pantalla inicial
{
    pantalla_Inicial *volver= new pantalla_Inicial;
    volver->show();
    close();

}
void MainWindow::fun_mov(){



    //CONDICION PARA SABER EN QUE RANGO LA PELOTA TIENE EL MOVIMIENTO PARABOLICO
    if(pelota->get_pel()->getY()<530 && pelota->get_pel()->getX()<1320&&pelota->get_pel()->getY()>-98){

                pelota->get_pel()->parabola();
                pelota->posicion(v_limit);

            }

            else{
            pelota->get_pel()->setvy(-pelota->get_pel()->getvy());
            pelota->get_pel()->parabola();
            pelota->posicion(v_limit);
                 }
            if(pelota->get_pel()->getX()>=1310){  //SI LA PELOTA SALE DE LA ESCENA CUENTA COMO TIRO

                tiros=tiros+1;
                cout<<"tiros "<<tiros<<endl;
                pelota->get_pel()->setValues(80,-35,110,110);
                pelota->posicion(v_limit);
                mov->stop();
                if(tiros==3){ //SI SE CUMPLEN LOS 3 TIROS EN EL NIVEL LE PASA AL SIGUIENTE NIVEL AUTOMATICAMENTE
                    lvl++;
                    fun_Nivel(lvl);
                    tiros=0;
                }

            }
            if(pelota->collidesWithItem(porte)){
            puntaje+=10;

            on_progressBar_valueChanged(puntaje);
            cout<<puntaje<<endl;
            med=new QMediaPlayer;
            med->setMedia(QUrl::fromLocalFile("/Users/MSI-PC/Desktop/Game/Futbol/y2mate.com - ahi_esta_gool_mariano_closs_-M-xbwWeA5k-[AudioTrimmer.com] (1).mp3"));
            med->setVolume(80);
            med->play();
            mov->start(20);
            }

            if(pelota->collidesWithItem(barrera)){
                tiros=tiros+1;

                pelota->get_pel()->setValues(80,-35,110,110);
                pelota->posicion(v_limit);
                mov->stop();
                if(tiros==3){
                    lvl++;
                    fun_Nivel(lvl);
                    tiros=0;
                }
            }

}

void MainWindow::on_pushButton_4_clicked() //BOTON GUARDAR PARTIDA
{

    ifstream basedatos;
    string line;
    string usuario_1="",clave_2="";
    string nivel="";
    string puntajes="";
    int tamano_cadena;
    int contador_espacios=0;
    basedatos.open("DATOS.txt", ios_base::app);
    if (basedatos.is_open()){

        while(basedatos.good()){ // En esta parte hago una copia de los usuarios en mapas, leo y guardo
            getline(basedatos,line); //Y luego elimino el archivo de texto

            tamano_cadena=line.size();
            for(int j=0; j<tamano_cadena;j++){
                if(line[j]==' ') contador_espacios++;

                if(line[j]!=' ' && contador_espacios == 0){
                    usuario_1=usuario_1+line[j];

                }
                if(line[j]!=' ' && contador_espacios ==1){
                    clave_2=clave_2+line[j];

                }
                if(line[j]!=' ' && contador_espacios ==2){
                    nivel=nivel+line[j];

                }
                if(line[j]!=' ' && contador_espacios==3){
                    puntajes=puntajes+line[j];
                }

            }

            usuarios[usuario_1]=clave_2;
            usuario_nivel[usuario_1]=nivel;
            usuario_puntaje[usuario_1]=puntajes;
            usuario_1="";clave_2="";nivel="";puntajes="";
            contador_espacios=0;

        }
        basedatos.close();
    }




    //NUEVA BASE DE DATOS

    remove("DATOS.txt");


    fstream base_datos;
    map<string,string>::iterator it;
    map<string,string>::iterator it_2;
    map<string,string>::iterator it_3;
    it= usuarios.begin();
    it_2= usuario_nivel.begin();
    it_3=usuario_puntaje.begin();
    string nivel1="1",nivel2="2",nivel3="3",nivel4="4";
    string cero="0",diez="10",veinte="20",treinta="30";
    string cuarenta="40",cincuenta="50",sesenta="60",setenta="70";
    string ochenta="80",noventa="90",cien="100",ciento_10="110";
    string ciento_20="120",ciento_30="130";

    base_datos.open("DATOS.txt", ios_base::app);
        if (base_datos.is_open()){ //Aca de pendiendo del nivel se guarda al jugador con su nuevo nivel y su puntaje
                                    // Se vuelve a crear la base de datos con los demas usuarios

            if(lvl==1){


                usuario_nivel[usuario_cargado_final]=nivel1;
                if(puntaje == 0){
                    usuario_puntaje[usuario_cargado_final]=cero;
                }
                if(puntaje == 10){
                    usuario_puntaje[usuario_cargado_final]=diez;
                }
                if(puntaje == 20){
                    usuario_puntaje[usuario_cargado_final]=veinte;

                }
                if(puntaje == 30){
                    usuario_puntaje[usuario_cargado_final]=treinta;
                }
                while(it!= usuarios.end() && it_2!= usuario_nivel.end() && it_3 != usuario_puntaje.end()){


                    base_datos<<it->first<<" "<<it->second<<" "<<it_2->second<<" "<<it_3->second<<"\n";
                    it++;
                    it_2++;
                    it_3++;

                }




            }
            if(lvl==2){


                usuario_nivel[usuario_cargado_final]=nivel2;
                if(puntaje == 0){
                    usuario_puntaje[usuario_cargado_final]=cero;
                }
                if(puntaje == 10){
                    usuario_puntaje[usuario_cargado_final]=diez;
                }
                if(puntaje == 20){
                    usuario_puntaje[usuario_cargado_final]=veinte;

                }
                if(puntaje == 30){
                    usuario_puntaje[usuario_cargado_final]=treinta;

                }
                if(puntaje == 40){
                    usuario_puntaje[usuario_cargado_final]=cuarenta;
                }
                if(puntaje == 50){
                    usuario_puntaje[usuario_cargado_final]=cincuenta;
                }
                if(puntaje == 60){
                    usuario_puntaje[usuario_cargado_final]=sesenta;
                }

                while(it!= usuarios.end() && it_2!= usuario_nivel.end() && it_3 != usuario_puntaje.end()){


                    base_datos<<it->first<<" "<<it->second<<" "<<it_2->second<<" "<<it_3->second<<"\n";
                    it++;
                    it_2++;
                    it_3++;

                }
            }
            if(lvl==3){


                usuario_nivel[usuario_cargado_final]=nivel3;

                if(puntaje == 0){
                    usuario_puntaje[usuario_cargado_final]=cero;
                }
                if(puntaje == 10){
                    usuario_puntaje[usuario_cargado_final]=diez;
                }
                if(puntaje == 20){
                    usuario_puntaje[usuario_cargado_final]=veinte;

                }
                if(puntaje == 30){
                    usuario_puntaje[usuario_cargado_final]=treinta;

                }
                if(puntaje == 40){
                    usuario_puntaje[usuario_cargado_final]=cuarenta;
                }
                if(puntaje == 50){
                    usuario_puntaje[usuario_cargado_final]=cincuenta;
                }
                if(puntaje == 60){
                    usuario_puntaje[usuario_cargado_final]=sesenta;
                }
                if(puntaje == 70){
                    usuario_puntaje[usuario_cargado_final]=setenta;

                }
                if(puntaje == 80){
                    usuario_puntaje[usuario_cargado_final]=ochenta;
                }
                if(puntaje == 90){
                    usuario_puntaje[usuario_cargado_final]=noventa;
                }
                while(it!= usuarios.end() && it_2!= usuario_nivel.end() && it_3 != usuario_puntaje.end()){


                    base_datos<<it->first<<" "<<it->second<<" "<<it_2->second<<" "<<it_3->second<<"\n";
                    it++;
                    it_2++;
                    it_3++;

                }
            }
            if(lvl==4){


                usuario_nivel[usuario_cargado_final]=nivel4;
                if(puntaje == 0){
                    usuario_puntaje[usuario_cargado_final]=cero;
                }
                if(puntaje == 10){
                    usuario_puntaje[usuario_cargado_final]=diez;
                }
                if(puntaje == 20){
                    usuario_puntaje[usuario_cargado_final]=veinte;


                }
                if(puntaje == 30){
                    usuario_puntaje[usuario_cargado_final]=treinta;
                }


                if(puntaje == 40){
                    usuario_puntaje[usuario_cargado_final]=cuarenta;
                }
                if(puntaje == 50){
                    usuario_puntaje[usuario_cargado_final]=cincuenta;
                }
                if(puntaje == 60){
                    usuario_puntaje[usuario_cargado_final]=sesenta;
                }
                if(puntaje == 70){
                    usuario_puntaje[usuario_cargado_final]=setenta;

                }
                if(puntaje == 80){
                    usuario_puntaje[usuario_cargado_final]=ochenta;
                }
                if(puntaje == 90){
                    usuario_puntaje[usuario_cargado_final]=noventa;
                }
                if(puntaje == 100){
                    usuario_puntaje[usuario_cargado_final]=cien;
                }
                if(puntaje == 110){
                    usuario_puntaje[usuario_cargado_final]=ciento_10;

                }
                if(puntaje == 120){
                    usuario_puntaje[usuario_cargado_final]=ciento_20;
                }
                if(puntaje == 130){
                    usuario_puntaje[usuario_cargado_final]=ciento_30;
                }

                while(it!= usuarios.end() && it_2!= usuario_nivel.end() && it_3 != usuario_puntaje.end()){


                    base_datos<<it->first<<" "<<it->second<<" "<<it_2->second<<" "<<it_3->second<<"\n";
                    it++;
                    it_2++;
                    it_3++;

                }
                }




    }
        base_datos.close();

}

void MainWindow::on_pushButton_3_clicked() //BOTON DE SALIR
{
    close();
}

void MainWindow::on_progressBar_valueChanged(int value) //BARRA DE PUNTAJE
{
    ui->progressBar->setValue(value);
}
