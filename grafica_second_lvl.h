#ifndef GRAFICA_SECOND_LVL_H
#define GRAFICA_SECOND_LVL_H
#include <QPainter>
#include <QGraphicsItem>
#include <QObject>
#include <QGraphicsPixmapItem>
#include <QGraphicsScene>
#include "second_lvl.h"


class GRAFICA_SECOND_LVL:public QObject,
        public QGraphicsPixmapItem
{
Q_OBJECT
public:
    GRAFICA_SECOND_LVL(QGraphicsItem* secc = 0);
    void posicion(float v_lim);
    second_lvl *getSec() const;
private:
    second_lvl *sec;
};

#endif // GRAFICA_SECOND_LVL_H
