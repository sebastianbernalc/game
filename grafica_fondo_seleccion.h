#ifndef GRAFICA_FONDO_SELECCION_H
#define GRAFICA_FONDO_SELECCION_H
#include <QPainter>
#include <QGraphicsItem>
#include <QObject>
#include <QGraphicsPixmapItem>
#include <QGraphicsScene>
#include "fondo_seleccion.h"


class GRAFICA_FONDO_SELECCION:public QObject,
        public QGraphicsPixmapItem
{
Q_OBJECT

public:
    GRAFICA_FONDO_SELECCION(QGraphicsItem* fonsell=0);
    FONDO_SELECCION *get_fonsel() const;

private:
    FONDO_SELECCION *fonsel;

};

#endif // GRAFICA_FONDO_SELECCION_H
