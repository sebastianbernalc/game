#include "first_lvl.h"

FIRST_LVL::FIRST_LVL()
{

}

int FIRST_LVL::getX() const
{
    return x;
}

int FIRST_LVL::getY() const
{
    return y;
}

int FIRST_LVL::getH() const
{
    return h;
}

int FIRST_LVL::getA() const
{
    return a;
}

void FIRST_LVL::setX(int value)
{
    x=value;
}

void FIRST_LVL::setY(int value)
{
    y=value;
}

void FIRST_LVL::setH(int value)
{
    h=value;
}

void FIRST_LVL::setA(int value)
{
    a=value;
}

void FIRST_LVL::setValues(int x1, int y1, int h1, int a1)
{
 x=x1,y=y1,h=h1,a=a1;
}
