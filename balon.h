#ifndef BALON_H
#define BALON_H


class BALON
{
public:
    BALON();
    int getX() const;
    int getY() const;
    void setX(int value);
    void setY(int value);
    void setValues(int x1, int y1,int h1, int a1);
    int getH() const;
    int getA() const;
    void setH(int value);
    void setA(int value);


private:
 int x=0, y=0,a=0,h=0;
};

#endif // BALON_H
