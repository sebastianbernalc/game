#include "pelota.h"
#define PI 3.14159265
#include <math.h>

PELOTA::PELOTA()
{

}

float PELOTA::getX() const
{
    return x;
}

float PELOTA::getY() const
{
    return y;
}

float PELOTA::getVEL() const
{
    return vel;
}

float PELOTA::getdt() const
{
    return dt;
}

void PELOTA::setdt(int value)
{
    dt=value;
}

void PELOTA::setVEL(int value)
{
    vel=value;
}

float PELOTA::getvy() const
{
    return vy;
}

float PELOTA::setvy(float value)
{
    vy=value;
}

float PELOTA::getvx() const
{
    return vx;
}

float PELOTA::setvx(float value)
{
    vx=value;
}

int PELOTA::getH() const
{
    return h;
}

int PELOTA::getA() const
{
    return a;

}

int PELOTA::getV() const
{
    return velocidad;
}

int PELOTA::getAN() const
{
    return angulo;
}

int PELOTA::getcons() const
{
    return cons;
}

void PELOTA::setcons(int value)
{
    cons=value;
}

void PELOTA::setV(int value)
{
    velocidad=value;
}

void PELOTA::setAN(int value)
{
 angulo=value;
}

void PELOTA::parabola()
{

    vy-=g*dt;
    x=x+vx*dt;
    y=y+vy*dt-(1/2)*(g*dt*dt);
}

void PELOTA::setVelocidades()
{
     vx=velocidad*cos((PI*angulo/180));
     vy=velocidad*sin(PI*angulo/180);

}

void PELOTA::rectilinio()
{
    x=x+vel*dt;
    y=y-vel*dt;
}

void PELOTA::caida_libre()
{
    y=y-vel*dt;
}

void PELOTA::circular()
{
    x=x+vel*cos((PI*angulo/180))*dt;
    y=y+vel*sin((PI*angulo/180))*dt;
    angulo=angulo+cons;
}

void PELOTA::setX(float value)
{
    x=value;
}

void PELOTA::setY(float value)
{
    y=value;
}

void PELOTA::setH(int value)
{
    h=value;
}

void PELOTA::setA(int value)
{
    a=value;
}

void PELOTA::setValues(float x1, float y1, int h1, int a1)
{
    x=x1;
    y=y1;
    h=h1;
    a=a1;
}
