#include "seleccion_de_juego.h"
#include "ui_seleccion_de_juego.h"
#include "mainwindow.h"

Seleccion_de_juego::Seleccion_de_juego(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Seleccion_de_juego)
{
    ui->setupUi(this);
    e=new QGraphicsScene;
    e->setSceneRect(0,0,h_limit,v_limit);
    ui->graphicsView->setScene(e);
    fondo=new GRAFICA_FONDO_SELECCION;
    fondo->get_fonsel()->setValues(0,0);
    e->addItem(fondo);
}

Seleccion_de_juego::~Seleccion_de_juego()
{
    delete ui;
}

bool Seleccion_de_juego::get_Selec() const
{
    return seleccion;
}

void Seleccion_de_juego::Pasar_lvl_pantalla(int value)
{
    LVL=value;
}

void Seleccion_de_juego::pasar_datos_usuario(string usuario_ca)
{
    usuario_cargad=usuario_ca;
}


void Seleccion_de_juego::on_pushButton_clicked() //Boton para cargar nivel en el que vaya el usuario
{

    MainWindow *mainwindow= new MainWindow(0);
    mainwindow->Pasar_Lvl(LVL);
    mainwindow->fun_Nivel(LVL);
    mainwindow->tener_datos_finales(usuario_cargad);
    mainwindow->show();
    close();

    seleccion=true;
}

void Seleccion_de_juego::on_pushButton_3_clicked()//Boton para cargar nueva partida en nivel 1
{

    MainWindow *mainwindow= new MainWindow(0);
    mainwindow->Pasar_Lvl(1);
    mainwindow->fun_Nivel(1);
    mainwindow->tener_datos_finales(usuario_cargad);
    mainwindow->show();
    close();

}

void Seleccion_de_juego::on_pushButton_2_clicked() // Boton para pasar a multijugador
{
    MainWindow *mainwindow= new MainWindow(0);
    mainwindow->Pasar_Lvl(1);
    mainwindow->fun_Nivel_Multijugador(1);
    mainwindow->tener_datos_finales(usuario_cargad);
    mainwindow->show();
    close();
}

void Seleccion_de_juego::on_pushButton_4_clicked() //Volver a pantalla incial
{
    pantalla_Inicial *volver= new pantalla_Inicial;
    volver->show();
    close();
}
