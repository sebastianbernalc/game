#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsScene>
#include "grafica.h"
#include "graficafondo.h"
#include "graficapelota.h"
#include <QTimer>
#include "graficaporteria.h"
#include <QKeyEvent>
#include "registro.h"
#include "seleccion_de_juego.h"
#include "grafica_barrera.h"
#include "pantalla_inicial.h"
#include "grafica_first_lvl.h"
#include "grafica_second_lvl.h"
#include "grafica_third.h"
#include "grafica_fourth.h"


#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include <QFileDialog>
#include <QDebug>
#include <QTextStream>
#include <QMediaPlayer>



namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    void keyPressEvent(QKeyEvent *event);
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    int h_limit=1340, v_limit=640;
    int lvl=0;
    int tiros=0;
    int tiros_total=0;
    void fun_Nivel(int value);
    void fun_Nivel_Multijugador(int value);
    void Pasar_Lvl(int value);
    string usuario_cargado_final="";
    map<string,string> usuarios;
    map<string,string> usuario_nivel;
    map<string,string> usuario_puntaje;
    void tener_datos_finales(string usuario_cargado);
    int c=0;



private slots:
    void on_pushButton_clicked();
    void fun_mov();
    void on_pushButton_2_clicked();
    void fun_mov2();
    void fun_tercerlvl();
    void fun_tercer_coll();
    void fun_cuartolvl();
    void fun_cuarto_coll();
    void fun_cuartolvl_Muilti();
    void fun_tercerlvl_Multi();
    void fun_mov2_Multi();
    void fun_mov_Multi();
    void fun_tercer_coll_Multi();
    void conectar_arduino();
    void control();
    void fun_cuarto_coll_Multi();





    void on_pushButton_4_clicked();

    void on_pushButton_3_clicked();

    void on_progressBar_valueChanged(int value);

private:
    Ui::MainWindow *ui;
    QGraphicsScene *scene;
    GRAFICA *balon;
    GRAFICAFONDO *fondo;
    GRAFICAPELOTA *pelota;
    GRAFICAPORTERIA *porte;
    Seleccion_de_juego *sel;
    GRAFICA_BARRERA *barrera;
    pantalla_Inicial *pan;
    GRAFICAPELOTA *pel1;
    GRAFICAPELOTA *pel2;
    GRAFICAPELOTA *pel3;
    GRAFICAPELOTA *pel4;
    GRAFICAPELOTA *pel5;
    GRAFICAPELOTA *pel6;
    GRAFICAPELOTA *pel7;
    GRAFICA_FIRST_LVL *first;
    GRAFICA_SECOND_LVL *second;
    QTimer *timer_control;
    QSerialPort *controll;
    QMediaPlayer *med;
    GRAFICA_THIRD *third;
    GRAFICA_FOURTH *fourth;

    int barrera_Aleatoria=0;
    int cont=0;
    int puntaje=0;



    QTimer *mov;
    QTimer *mov2;

};

#endif // MAINWINDOW_H
