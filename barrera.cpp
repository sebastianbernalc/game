#include "barrera.h"

BARRERA::BARRERA()
{

}

int BARRERA::getX() const
{
    return x;
}

int BARRERA::getY() const
{
    return y;
}

int BARRERA::getH() const
{
    return h;
}

int BARRERA::getA() const
{
    return a;
}

int BARRERA::getV() const
{
    return v;
}

void BARRERA::setV(int value)
{
    v=value;
}

void BARRERA::setX(int value)
{
    x=value;
}

void BARRERA::setY(int value)
{
    y=value;
}

void BARRERA::setH(int value)
{
    h=value;
}

void BARRERA::setA(int value)
{
    a=value;
}

void BARRERA::setValues(int x1, int y1, int h1, int a1)
{
    x=x1,y=y1,h=h1,a=a1;
}

void BARRERA::movimiento()
{
    x=x+v*dt;

}
