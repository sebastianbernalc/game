#ifndef GRAFICAFONDO_H
#define GRAFICAFONDO_H
#include <QPainter>
#include <QGraphicsItem>
#include <QObject>
#include <QGraphicsPixmapItem>
#include <QGraphicsScene>
#include "fondo.h"

class GRAFICAFONDO:public QObject,
        public QGraphicsPixmapItem
{
Q_OBJECT
public:
    GRAFICAFONDO(QGraphicsItem* fonn=0);
    FONDO *get_fon() const;

private:
    FONDO *fon;


};

#endif // GRAFICAFONDO_H
