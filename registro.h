#ifndef REGISTRO_H
#define REGISTRO_H

#include <QDialog>
#include <QGraphicsScene>
#include <string>
#include<iostream>
#include <fstream>
#include <QWidget>
#include "grafica_fondo_seleccion.h"
using namespace std;

namespace Ui {
class Registro;
}

class Registro : public QDialog
{
    Q_OBJECT

public:
    explicit Registro(QWidget *parent = 0);
    ~Registro();
    int h_limit=1000, v_limit=488;

private slots:
    void on_buttonBox_accepted();

private:
    Ui::Registro *ui;
    QGraphicsScene *e;
    QString usuario,contrasena,contrasena_2;
    GRAFICA_FONDO_SELECCION *fondo;

};

#endif // REGISTRO_H
