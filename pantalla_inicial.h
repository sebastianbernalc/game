#ifndef PANTALLA_INICIAL_H
#define PANTALLA_INICIAL_H

#include <QWidget>
#include <QGraphicsScene>
#include "grafica_pantalla_inicio.h"
#include "registro.h"
#include "seleccion_de_juego.h"
#include <string>
#include<iostream>
#include <fstream>
#include <QMediaPlayer>


namespace Ui {
class pantalla_Inicial;
}

class pantalla_Inicial : public QWidget
{
    Q_OBJECT

public:
    explicit pantalla_Inicial(QWidget *parent = 0);
    ~pantalla_Inicial();
    int h_limit=1000, v_limit=488;
    int nivel_de_juego=0;
    string usuario_cargado="";




private slots:
    void on_pushButton_2_clicked();

    void on_pushButton_clicked();

private:
    Ui::pantalla_Inicial *ui;
    GRAFICA_PANTALLA_INICIO *inicio;
    QGraphicsScene *e;
    QString usuario,contrasena;
    QMediaPlayer *Mmedia;

};

#endif // PANTALLA_INICIAL_H
