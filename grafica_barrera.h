#ifndef GRAFICA_BARRERA_H
#define GRAFICA_BARRERA_H
#include <QPainter>
#include <QGraphicsItem>
#include <QObject>
#include <QGraphicsPixmapItem>
#include <QGraphicsScene>
#include "barrera.h"


class GRAFICA_BARRERA :public QObject,
        public QGraphicsPixmapItem
{
 Q_OBJECT
public:
    GRAFICA_BARRERA(QGraphicsItem* barr = 0);
    void posicion(float v_lim);
    BARRERA *getBar() const;
    void movimiento();
private:
    BARRERA *bar;
};

#endif // GRAFICA_BARRERA_H
