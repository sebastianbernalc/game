#ifndef FONDO_SELECCION_H
#define FONDO_SELECCION_H


class FONDO_SELECCION
{
public:
    FONDO_SELECCION();
    int getX() const;
    int getY() const;
    void setX(int value);
    void setY(int value);
    void setValues(int x1, int y1);
private:
    int x=0, y=0;
};

#endif // FONDO_SELECCION_H
