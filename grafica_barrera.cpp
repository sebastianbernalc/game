#include "grafica_barrera.h"

GRAFICA_BARRERA::GRAFICA_BARRERA(QGraphicsItem *barr):QGraphicsPixmapItem(barr)
{
    setPixmap(QPixmap(":/QM3D3094 (1) (1).png"));
    bar= new BARRERA;
}

void GRAFICA_BARRERA::posicion(float v_lim)
{
   setPos(bar->getX(),(v_lim-bar->getY()-bar->getH()));

}

BARRERA *GRAFICA_BARRERA::getBar() const
{
    return bar;
}

void GRAFICA_BARRERA::movimiento()
{
    bar->movimiento();
}
