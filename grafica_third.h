#ifndef GRAFICA_THIRD_H
#define GRAFICA_THIRD_H
#include "third_lvl.h"
#include <QPainter>
#include <QGraphicsItem>
#include <QObject>
#include <QGraphicsPixmapItem>
#include <QGraphicsScene>

class GRAFICA_THIRD:public QObject,
        public QGraphicsPixmapItem
{
Q_OBJECT

public:
    GRAFICA_THIRD(QGraphicsItem* thirr = 0);
    void posicion(float v_lim);
    Third_lvl *get_thir() const;
private:
    Third_lvl *thir;
};

#endif // GRAFICA_THIRD_H
