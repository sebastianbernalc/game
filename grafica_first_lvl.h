#ifndef GRAFICA_FIRST_LVL_H
#define GRAFICA_FIRST_LVL_H
#include <QPainter>
#include <QGraphicsItem>
#include <QObject>
#include <QGraphicsPixmapItem>
#include <QGraphicsScene>
#include "first_lvl.h"

class GRAFICA_FIRST_LVL:public QObject,
        public QGraphicsPixmapItem
{
Q_OBJECT
public:

    GRAFICA_FIRST_LVL(QGraphicsItem* firr = 0);
    void posicion(float v_lim);
    FIRST_LVL *getFir() const;
private:
    FIRST_LVL *fir;
};

#endif // GRAFICA_FIRST_LVL_H
