#ifndef GRAFICA_PANTALLA_INICIO_H
#define GRAFICA_PANTALLA_INICIO_H
#include "pantalla_inicio.h"
#include <QPainter>
#include <QGraphicsItem>
#include <QObject>
#include <QGraphicsPixmapItem>
#include <QGraphicsScene>

class GRAFICA_PANTALLA_INICIO:public QObject,
        public QGraphicsPixmapItem
{
public:
    GRAFICA_PANTALLA_INICIO(QGraphicsItem* pann=0);
    PANTALLA_INICIO *get_pan() const;

private:
    PANTALLA_INICIO *pan;
};

#endif // GRAFICA_PANTALLA_INICIO_H
