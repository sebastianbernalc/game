#ifndef PATEADOR_H
#define PATEADOR_H
#include <QGraphicsItem>
#include <QObject>
#include <QTimer>
#include <QPixmap>

class PATEADOR : public QObject, public QGraphicsItem
{
    Q_OBJECT
public:
    explicit PATEADOR(QObject *parent = nullptr);
    QTimer *timer;
    QPixmap *pixmap;

signals:

public slots:
};

#endif // PATEADOR_H
