#include "second_lvl.h"

second_lvl::second_lvl()
{

}

int second_lvl::getX() const
{
    return x;
}

int second_lvl::getY() const
{
    return y;
}

int second_lvl::getH() const
{
    return h;
}

int second_lvl::getA() const
{
    return a;
}

void second_lvl::setX(int value)
{
    x=value;
}

void second_lvl::setY(int value)
{
    y=value;
}

void second_lvl::setH(int value)
{
    h=value;
}

void second_lvl::setA(int value)
{
    a=value;
}

void second_lvl::setValues(int x1, int y1, int h1, int a1)
{
    x=x1,y=y1,a=a1,h=h1;
}
