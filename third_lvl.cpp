#include "third_lvl.h"

Third_lvl::Third_lvl()
{

}

int Third_lvl::getX() const
{
    return x;
}

int Third_lvl::getY() const
{
    return y;
}

int Third_lvl::getH() const
{
    return h;
}

int Third_lvl::getA() const
{
    return a;
}

void Third_lvl::setX(int value)
{
    x=value;
}

void Third_lvl::setY(int value)
{
    y=value;
}

void Third_lvl::setH(int value)
{
    h=value;
}

void Third_lvl::setA(int value)
{
    a=value;
}

void Third_lvl::setValues(int x1, int y1, int h1, int a1)
{
 x=x1,y=y1,a=a1,h=h1;
}
