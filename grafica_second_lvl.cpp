#include "grafica_second_lvl.h"

GRAFICA_SECOND_LVL::GRAFICA_SECOND_LVL(QGraphicsItem *secc):QGraphicsPixmapItem(secc)
{
    setPixmap(QPixmap(":/New Project (4).png"));
    sec= new second_lvl;
}

void GRAFICA_SECOND_LVL::posicion(float v_lim)
{
    setPos(sec->getX(),(v_lim-sec->getY()-sec->getH()));
}

second_lvl *GRAFICA_SECOND_LVL::getSec() const
{
    return sec;
}
