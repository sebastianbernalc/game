#ifndef FONDO_H
#define FONDO_H


class FONDO
{
public:
    FONDO();
    int getX() const;
    int getY() const;
    void setX(int value);
    void setY(int value);
    void setValues(int x1, int y1);
private:
    int x=0, y=0;
};

#endif // FONDO_H
