#include "porteria.h"

PORTERIA::PORTERIA()
{

}

int PORTERIA::getX() const
{
    return x;
}

int PORTERIA::getY() const
{
    return y;
}

int PORTERIA::getH() const
{
    return h;

}

int PORTERIA::getA() const
{
    return a;
}

int PORTERIA::getV() const
{
    return v;
}

void PORTERIA::setV(int value)
{
    v=value;
}

void PORTERIA::setX(int value)
{
    x=value;
}

void PORTERIA::setY(int value)
{
    y=value;
}

void PORTERIA::setH(int value)
{
    h=value;
}

void PORTERIA::setA(int value)
{
    a=value;
}

void PORTERIA::setValues(int x1, int y1, int h1, int a1)
{
    x=x1;
    y=y1;
    h=h1;
    a=a1;
}

void PORTERIA::movimiento()
{
    y=y+v*dt;
}


