#ifndef PELOTA_H
#define PELOTA_H


class PELOTA
{
public:
    PELOTA();
    float getX() const;
    float getY() const;
    float getVEL() const;
    float getdt()const;
    void setdt(int value);
    void setVEL(int value);
    float getvy()const;
    float setvy(float value);
    float getvx()const;
    float setvx(float value);
    int getH() const;
    int getA() const;
    int getV()const;
    int getAN()const;
    int getcons()const;
    void setcons(int value);
    void setV(int value);
    void setAN(int value);
    void setX(float value);
    void setY(float value);
    void setH(int value);
    void setA(int value);
    void setValues(float x1, float y1, int h1, int a1);
    void parabola();
    void setVelocidades();
    void rectilinio();
    void caida_libre();
    void circular();
private:
    float x=0, y=0, h=0, a=0, dt =0.1, vx=0, vy=0, g=10,ac=30, vel=25;
    int velocidad=0, angulo=0,r=5,cons=2;
};

#endif // PELOTA_H
