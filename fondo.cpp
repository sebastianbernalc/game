#include "fondo.h"

FONDO::FONDO()
{

}

int FONDO::getX() const
{
    return x;
}

int FONDO::getY() const
{
    return y;
}

void FONDO::setX(int value)
{
    x=value;
}

void FONDO::setY(int value)
{
    y=value;
}

void FONDO::setValues(int x1, int y1)
{
 x=x1;
 y=y1;
}
