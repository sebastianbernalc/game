#ifndef FIRST_LVL_H
#define FIRST_LVL_H


class FIRST_LVL
{
public:
    FIRST_LVL();
    int getX() const;
    int getY() const;
    int getH() const;
    int getA() const;

    void setX(int value);
    void setY(int value);
    void setH(int value);
    void setA(int value);
    void setValues(int x1, int y1, int h1, int a1);
private:
    float x=0, y=0, h=0, a=0;
};

#endif // FIRST_LVL_H
