#ifndef GRAFICAPORTERIA_H
#define GRAFICAPORTERIA_H
#include <QPainter>
#include <QGraphicsItem>
#include <QObject>
#include <QGraphicsPixmapItem>
#include <QGraphicsScene>
#include "porteria.h"


class GRAFICAPORTERIA:public QObject,
        public QGraphicsPixmapItem
{
        Q_OBJECT
public:

    GRAFICAPORTERIA (QGraphicsItem* porr = 0);
    void posicion(float v_lim);
    PORTERIA *getPor() const;
    void movimiento();

private:
     PORTERIA *por;

};

#endif // GRAFICAPORTERIA_H
