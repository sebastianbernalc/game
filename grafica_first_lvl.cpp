#include "grafica_first_lvl.h"



GRAFICA_FIRST_LVL::GRAFICA_FIRST_LVL(QGraphicsItem *firr):QGraphicsPixmapItem(firr)
{
    setPixmap(QPixmap(":/New Project (3).png"));
    fir= new FIRST_LVL;
}

void GRAFICA_FIRST_LVL::posicion(float v_lim)
{
    setPos(fir->getX(),(v_lim-fir->getY()-fir->getH()));
}

FIRST_LVL *GRAFICA_FIRST_LVL::getFir() const
{
 return fir;
}
