#include "registro.h"
#include "ui_registro.h"
#include "iostream"
#include "pantalla_inicial.h"

#include <fstream>
#include <ostream>
#include <string>

using namespace std;
Registro::Registro(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Registro)
{
    ui->setupUi(this);
    e=new QGraphicsScene;
    e->setSceneRect(0,0,h_limit,v_limit);     //asigna el rectangulo que encierra la scene, determinado por h_limit y v_limit
    ui->graphicsView->setScene(e);
    fondo=new GRAFICA_FONDO_SELECCION;
    fondo->get_fonsel()->setValues(0,0);
    e->addItem(fondo);

}

Registro::~Registro()
{
    delete ui;
}

void Registro::on_buttonBox_accepted()
{
    //Esta es la parte que registra a la persona

    usuario=ui->lineEdit->text();
    contrasena=ui->lineEdit_2->text();
    contrasena_2=ui->lineEdit_3->text();
    //Creaccion de base de datos
    string line;
    bool usuario_registrado=false;
    int tamano_usuario=usuario.size();
    int tamano_contrasena=contrasena.size();
    int contador_usuario=0,contador_clave=0;

    ifstream base_datos;

    base_datos.open("DATOS.TXT");
    if (base_datos.is_open()){ //Aqui se mira si el jugador esta ya registrado

        while(base_datos.good()){
            getline (base_datos,line);



            for(int i=0;i<tamano_usuario;i++){

                if(line[i]==usuario[i]){
                    contador_usuario++;

                }



            }
            for(int i=0;i<tamano_contrasena;i++){
                if(line[i+tamano_usuario+1]==contrasena[i]){
                    contador_clave++;

                }
            }
            if(contador_usuario==tamano_usuario && contador_clave==tamano_contrasena){

                usuario_registrado=true;

            }
            else{
                contador_clave=0;contador_usuario=0;

            }

        }




    }
    base_datos.close();
    //Sino esta registrado pasa a registrarlo en la base de datos
    fstream basedatos;
    int contador_clave_registro=0;
    basedatos.open("DATOS.TXT", ios_base::app);
    if (basedatos.is_open()){

        if(usuario_registrado==false){
            for(int i=0;i<tamano_contrasena;i++){
                if(contrasena[i]==contrasena_2[i]){
                    contador_clave_registro++;
                }

            }
            if(contador_clave_registro==tamano_contrasena){
               basedatos<<usuario.toStdString();
               basedatos<<" "<<contrasena.toStdString()<<" ";
               basedatos<<"1\n";
            }
        }
        basedatos.close();
    }
    pantalla_Inicial *volver_= new pantalla_Inicial;
    volver_->show();
    close();


}
