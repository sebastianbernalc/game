#include "grafica_fourth.h"


GRAFICA_FOURTH::GRAFICA_FOURTH(QGraphicsItem *fourr):QGraphicsPixmapItem(fourr)
{
    setPixmap(QPixmap(":/New Project (6).png"));
    four= new Fourth_lvl;
}


void GRAFICA_FOURTH::posicion(float v_lim)
{
    setPos(four->getX(),(v_lim-four->getY()-four->getH()));
}

Fourth_lvl *GRAFICA_FOURTH::getFour() const
{
    return four;
}
