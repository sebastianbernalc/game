#ifndef GRAFICA_H
#define GRAFICA_H

#include <QPainter>
#include <QGraphicsItem>
#include <QObject>
#include <QGraphicsPixmapItem>
#include <QGraphicsScene>
#include <balon.h>

class GRAFICA: public QObject,
               public QGraphicsPixmapItem
{
Q_OBJECT
public:
    GRAFICA(QGraphicsItem* ball = 0);
    BALON *get_bal() const;
    void posicion(float v_lim);
private:
    BALON *bal;
};


#endif // GRAFICA_H
