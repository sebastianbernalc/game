#ifndef SELECCION_DE_JUEGO_H
#define SELECCION_DE_JUEGO_H
#include "pantalla_inicial.h"
#include <QGraphicsScene>
#include <QWidget>
#include <grafica_fondo_seleccion.h>

namespace Ui {
class Seleccion_de_juego;
}

class Seleccion_de_juego : public QWidget
{
    Q_OBJECT

public:
    explicit Seleccion_de_juego(QWidget *parent = 0);
    ~Seleccion_de_juego();
    bool seleccion;
    bool get_Selec()const;
    int LVL;
    string usuario_cargad="";
    void Pasar_lvl_pantalla(int value);
    void pasar_datos_usuario(string usuario_ca);
    int h_limit=1000, v_limit=488;


private slots:
    void on_pushButton_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_4_clicked();

private:
    Ui::Seleccion_de_juego *ui;
    QGraphicsScene *e;
    GRAFICA_FONDO_SELECCION *fondo;


};

#endif // SELECCION_DE_JUEGO_H
