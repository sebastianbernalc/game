#ifndef PANTALLA_INICIO_H
#define PANTALLA_INICIO_H


class PANTALLA_INICIO
{
public:
    PANTALLA_INICIO();
    int getX() const;
    int getY() const;
    void setX(int value);
    void setY(int value);
    void setValues(int x1, int y1);
private:
    int x,y;
};

#endif // PANTALLA_INICIO_H
