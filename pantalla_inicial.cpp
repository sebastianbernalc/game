#include "pantalla_inicial.h"
#include "ui_pantalla_inicial.h"
#include "registro.h"

using namespace std;


pantalla_Inicial::pantalla_Inicial(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::pantalla_Inicial)
{
    ui->setupUi(this);
    Mmedia=new QMediaPlayer;
    e=new QGraphicsScene;
    e->setSceneRect(0,0,h_limit,v_limit);     //asigna el rectangulo que encierra la scene, determinado por h_limit y v_limit
    ui->graphicsView->setScene(e);
    inicio=new GRAFICA_PANTALLA_INICIO;
    inicio->get_pan()->setValues(0,0);
    e->addItem(inicio);
    Mmedia=new QMediaPlayer;
    Mmedia->setMedia(QUrl::fromLocalFile("/Users/MSI-PC/Desktop/Game/Futbol/Kasabian - Club Foot-[AudioTrimmer.com].mp3"));
    connect(Mmedia,SIGNAL(positionChanged(qint64)),this,SLOT(positionChanged(quint64)));
    Mmedia->setVolume(80);
    Mmedia->play();

}

pantalla_Inicial::~pantalla_Inicial()
{
    delete ui;
}



void pantalla_Inicial::on_pushButton_2_clicked() //Boton para verificar si el usuario esta registrado
{
    usuario=ui->lineEdit->text();
    contrasena=ui->lineEdit_2->text();
    //Creaccion de base de datos
    string line;
    int tamano_cadena;
    int tamano_usuario=usuario.size();
    int tamano_contrasena=contrasena.size();
    int contador_usuario=0,contador_clave=0;

    ifstream base_datos;

    base_datos.open("DATOS.TXT");
    if (base_datos.is_open()){ //Se mira si el usuario y la clave estan en la base de datos

        while(base_datos.good()){
            getline (base_datos,line);


            tamano_cadena=line.size();
            for(int i=0;i<tamano_usuario;i++){

                if(line[i]==usuario[i]){
                    contador_usuario++;

                }



            }
            for(int i=0;i<tamano_contrasena;i++){
                if(line[i+tamano_usuario+1]==contrasena[i]){
                    contador_clave++;

                }
            }
            if(contador_usuario==tamano_usuario && contador_clave==tamano_contrasena){

                //Si lo estan se pasa al nivel donde este


                                            if(line[tamano_usuario+tamano_contrasena+2]=='1'){
                                                nivel_de_juego=1;


                                                usuario_cargado=usuario.toStdString();



                                            }
                                            else if (line[tamano_usuario+tamano_contrasena+2]=='2') {
                                                pantalla_Inicial::nivel_de_juego=2;




                                                usuario_cargado=usuario.toStdString();

                                            }
                                            else if (line[tamano_usuario+tamano_contrasena+2]=='3') {
                                                pantalla_Inicial::nivel_de_juego=3;



                                                usuario_cargado=usuario.toStdString();


                                            }
                                            else if (line[tamano_usuario+tamano_contrasena+2]=='4') {
                                                pantalla_Inicial::nivel_de_juego=4;


                                                usuario_cargado=usuario.toStdString();




                                            }

                                            Seleccion_de_juego *seleccion_de_juego= new Seleccion_de_juego(0);
                                            seleccion_de_juego->show();
                                            seleccion_de_juego->Pasar_lvl_pantalla(nivel_de_juego);
                                            seleccion_de_juego->pasar_datos_usuario(usuario_cargado);









                        }

            else{
                contador_clave=0;contador_usuario=0;
            }

        }
    }
Mmedia->stop();
close();
}
    


void pantalla_Inicial::on_pushButton_clicked() //Se manda a la ventana de registro
{
    Registro *registro= new Registro(0);
    registro->show();
    Mmedia->play();
    close();
}

